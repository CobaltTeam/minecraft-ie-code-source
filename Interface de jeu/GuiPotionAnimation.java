package net.minecraft.client.gui;

import java.util.Collection;
import java.util.Iterator;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.StringUtils;
import net.minecraft.util.text.TextFormatting;

public class GuiPotionAnimation extends Gui
{
    private Minecraft mc;
    private int width;
    private int height;
    private long tick;
    private boolean currentState;
    boolean close;
    
    public GuiPotionAnimation(Minecraft mc)
    {
        this.mc = mc;
    }
    
    public boolean getCurrentState()
    {
		return currentState;  	
    }

    public void setPotionInstance(boolean pbool_close, boolean pbool_currentstate)
    {
        this.tick = Minecraft.getSystemTime();
        this.close = pbool_close;
        this.currentState = pbool_currentstate;
    }
    
    private void updatePotionWindowScale()
    {
        GlStateManager.viewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
        GlStateManager.matrixMode(5889);
        GlStateManager.loadIdentity();
        GlStateManager.matrixMode(5888);
        GlStateManager.loadIdentity();
        this.width = this.mc.displayWidth;
        this.height = this.mc.displayHeight;
        ScaledResolution scaledresolution = new ScaledResolution(this.mc);
        this.width = scaledresolution.getScaledWidth();
        this.height = scaledresolution.getScaledHeight();
        GlStateManager.clear(256);
        GlStateManager.matrixMode(5889);
        GlStateManager.loadIdentity();
        GlStateManager.ortho(0.0D, (double)this.width, (double)this.height, 0.0D, 1000.0D, 3000.0D);
        GlStateManager.matrixMode(5888);
        GlStateManager.loadIdentity();
        GlStateManager.translate(0.0F, 0.0F, -2000.0F);
    }

    public void updatePotionWindow()
    {
        if (this.tick != 0L)
        {
            double d0 = (double)(Minecraft.getSystemTime() - this.tick) / (this.close ? 100.0D : 3000.0D);
            if (this.close)
            {
                if (d0 < 0.0D || d0 > 1.0D)
                {
                	this.tick = 0L;
                    return;
                }
            }
            else if (d0 > 0.5D)
            {
                d0 = 0.5D;
            }

            this.updatePotionWindowScale();
            GlStateManager.disableDepth();
            GlStateManager.depthMask(false);
            double d1 = d0 * 2.0D;

            if (d1 > 1.0D)
            {
                d1 = 2.0D - d1;
            }

            d1 = d1 * 4.0D;
            d1 = 1.0D - d1;

            if (d1 < 0.0D)
            {
                d1 = 0.0D;
            }

            d1 = d1 * d1;
            d1 = d1 * d1;
            int i = this.width - 160;
            int j = this.width - 35 + (int)(d1 * 36.0D);

            int width = this.close ? this.width - 40 + (int) (d0 * 70): j;
            int height = 40;   
            GlStateManager.pushMatrix();
            Collection activepotion = this.mc.thePlayer.getActivePotionEffects();
            
            if (!activepotion.isEmpty())
            {
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);        
                int width2 = 33;

                for (Iterator potionsIterator = this.mc.thePlayer.getActivePotionEffects().iterator(); potionsIterator.hasNext(); height += width2)
                {
                    PotionEffect potionEffect = (PotionEffect)potionsIterator.next();
                    Potion potion = potionEffect.getPotion();

                    GlStateManager.enableBlend();
                  	if (mc.gameSettings.White){
                		GlStateManager.color(1.0F, 1.0F, 1.0F, mc.gameSettings.inventorytransparency ? 0.5F : 1.0F);
                    }
                	if (mc.gameSettings.Black) {
                		GlStateManager.color(0.3F, 0.3F, 0.3F, mc.gameSettings.inventorytransparency ? 0.5F : 1.0F);
                	}
                	if (mc.gameSettings.Red){
                		GlStateManager.color(1.0F, 0.0F, 0.0F, mc.gameSettings.inventorytransparency ? 0.5F : 1.0F);
                    }
                	if (mc.gameSettings.Green){
                		GlStateManager.color(0.0F, 1.0F, 0.0F, mc.gameSettings.inventorytransparency ? 0.5F : 1.0F);
                    }
                	if (mc.gameSettings.Yellow){
                		GlStateManager.color(1.0F, 1.0F, 0.0F, mc.gameSettings.inventorytransparency ? 0.5F : 1.0F);
                    }
                	if (mc.gameSettings.Blue){
                		GlStateManager.color(0.0F, 0.0F, 1.0F, mc.gameSettings.inventorytransparency ? 0.5F : 1.0F);
                    }
                	if (mc.gameSettings.LightPurple){
                		GlStateManager.color(1.0F, 0.0F, 1.0F, mc.gameSettings.inventorytransparency ? 0.5F : 1.0F);
                    }
                	if (mc.gameSettings.LightBlue){
                		GlStateManager.color(0.0F, 1.0F, 1.0F, mc.gameSettings.inventorytransparency ? 0.5F : 1.0F);
                    } 
                	if (mc.gameSettings.MinecraftPE){
                        GlStateManager.color(1.0F, 1.0F, 0.0F, 1.0F);
                	}  
                	if (mc.gameSettings.resourcepackcustom || mc.gameSettings.resourcepackType1 || mc.gameSettings.Minecraft){
                        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                	}  

                    this.mc.getTextureManager().bindTexture(GuiInventory.INVENTORY_BACKGROUND);
                    this.drawTexturedModalRect(width, height, 0, 166, 15, 32);                
                    this.drawTexturedModalRect(width + 15, height, 105, 166, 20, 32);         

                    if (potion.hasStatusIcon()) {
                        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                        int iconpath = potion.getStatusIconIndex();
                        this.drawTexturedModalRect(width + 6, height + 4, 0 + iconpath % 8 * 18, 198 + iconpath / 8 * 18, 18, 18);
                    }                   
                    
                    String name = "";
                    if (potionEffect.getAmplifier() == 1)
                    {
                        name = TextFormatting.BOLD + I18n.format("enchantment.level.2", new Object[0]);
                    }
                    else if (potionEffect.getAmplifier() == 2)
                    {
                        name = TextFormatting.BOLD + I18n.format("enchantment.level.3", new Object[0]);
                    }
                    else if (potionEffect.getAmplifier() == 3)
                    {
                        name = TextFormatting.BOLD + I18n.format("enchantment.level.4", new Object[0]);
                    }
                    this.mc.fontRendererObj.drawStringWithShadow(name,width + 18,height + 1, 16777215);
                    String duration = this.getDurationString(potionEffect);
                    duration = potion.isBadEffect() ? TextFormatting.DARK_RED + duration : TextFormatting.WHITE + duration;               
                    this.mc.fontRendererObj.drawStringWithShadow(duration,width + 6,height + 21, 8355711);
                }
            }
            GlStateManager.popMatrix();
            GlStateManager.enableDepth();
            GlStateManager.depthMask(true);
        }
    }

    public static String getDurationString(PotionEffect effect)
    {
        if (effect.getIsPotionDurationMax())
        {
            return "**:**";
        }
        else
        {
            int i = effect.getDuration();
            return StringUtils.ticksToElapsedTime(i);
        }
    }
}

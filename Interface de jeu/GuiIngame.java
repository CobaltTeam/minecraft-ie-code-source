public class GuiIngame extends Gui
{
	/*FAIT LE RENDU DE L'ITEM DANS L'INTERFACE*/
    private void renderItemGui(ItemStack itemStack, int x, int y, float f, boolean overlay)
    {
    	if (itemStack != null)
    	{
    	    float anim = (float)itemStack.animationsToGo - f;
            if (anim > 0.0F)
            {
                float dec = 1.0F + anim / 5.0F;
                GlStateManager.translate((float)(x + 8), (float)(y + 12), 0.0F);
                GlStateManager.scale(1.0F / dec, (dec + 1.0F) / 2.0F, 1.0F);
                GlStateManager.translate((float)(-(x + 8)), (float)(-(y + 12)), 0.0F);
            }
            	itemRenderer.renderItemIntoGUI(itemStack, x, y);
            	
            if (anim > 0.0F)
            {
            	GlStateManager.pushMatrix();
            }
            if (overlay)
            {
                this.itemRenderer.renderItemOverlays(this.getFontRenderer(), itemStack, x, y);
            }
            if (anim > 0.0F)
            {
            	GlStateManager.popMatrix();
            }	
            
            GlStateManager.disableLighting();
    	}
    }
    
	/* LA M�THODE POUR FAIRE LE RENDU DE L'ARMURE*/
    public void renderArmor()
	{
	   	 ScaledResolution scaledresolution = new ScaledResolution(this.mc);
	   	 int i = scaledresolution.getScaledWidth();
	   	 int j = scaledresolution.getScaledHeight();
		 EntityPlayer entityplayer = (EntityPlayer)this.mc.getRenderViewEntity();
		 ItemStack itemstack = entityplayer.getHeldItemOffhand();
		 EnumHandSide enumhandside = entityplayer.getPrimaryHand().opposite(); 
		 if (this.mc.gameSettings.ArmorHudDesign == 1)
		 {
			 for (int slotarmor = 0; slotarmor < 2; ++slotarmor)
			 {
			    int width = i / 2 - 150;
			    int height = j - 16;
			    ItemStack itemStack = this.mc.thePlayer.inventory.armorItemInSlot(4 - slotarmor - 1);
				
			    if (itemStack == null)
			    {
			    	continue;
			    }
			          
			    if (itemstack != null && enumhandside == EnumHandSide.LEFT)
			    {
			        width = i / 2 - 180;
				    height = j - 16; 
			    }
			    double damage = new Double(itemStack.getMaxDamage());
			    double result = itemStack.getItemDamage() / damage;
			    double finalResult = result * 100;
			    DecimalFormat decimalformat = new DecimalFormat("###");
			    int[] frame  = {20, 40, 60, 80};
			    String[] colors_degrement = {"�4", "�c", "�6", "�a", "�2"};
			    int stage = 0;
			                
			    for (stage = 0; stage < frame.length; ++stage)
			    {
			         if (100 - finalResult < frame[stage])
			         {
			              break;
			         }
			    }
			                
			     GlStateManager.pushMatrix();
				 GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
				 this.renderItemGui(itemStack,width + 40, slotarmor * 15 + height - 17,  i, true);
				 GlStateManager.popMatrix();
			     String current_status = decimalformat.format(100 - finalResult) + " %" + TextFormatting.RESET ;
			     int var9 = 8453920;
			     this.getFontRenderer().drawString(current_status,width + 18 - 5, slotarmor * 15 + height - 17 + 7, 0);
			     this.getFontRenderer().drawString(current_status,width + 18 - 7, slotarmor * 15 + height - 17 + 7, 0);
			     this.getFontRenderer().drawString(current_status,width + 18 - 6, slotarmor * 15 + height - 17 + 8, 0);
			     this.getFontRenderer().drawString(current_status,width + 18 - 6, slotarmor * 15 + height - 17 + 6, 0);
			     this.getFontRenderer().drawString(colors_degrement[stage] + current_status,width + 18 - 6,slotarmor * 15 + height - 17 + 7, var9);
			 }
			 
	     	for (int slotarmor = 2; slotarmor < 4; ++slotarmor)
	     	{
	     		int width = i / 2 + 55;
	     		int height = j - 15;
	     	    ItemStack itemStack = this.mc.thePlayer.inventory.armorItemInSlot(4 - slotarmor - 1);
	     			
	     	    if (itemStack == null)
			    {
			    	continue;
			    }
			          
		          if (itemstack != null && enumhandside == EnumHandSide.RIGHT)
		          {
		        	  width = i / 2 + 85;
			     	  height = j - 15; 
		          }
			    
			    double damage = new Double(itemStack.getMaxDamage());
			    double result = itemStack.getItemDamage() / damage;
			    double finalResult = result * 100;
			    DecimalFormat decimalformat = new DecimalFormat("###");
			    int[] frame  = {20, 40, 60, 80};
			    String[] colors_degrement = {"�4", "�c", "�6", "�a", "�2"};
			    int stage = 0;
			                
			    for (stage = 0; stage < frame.length; ++stage)
			    {
			         if (100 - finalResult < frame[stage])
			         {
			              break;
			         }
			    }
			                
			     GlStateManager.pushMatrix();
				 GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		         this.renderItemGui(itemStack, width + 40,  slotarmor * 15 + height - 17 - 30,  i, true);
				 GlStateManager.popMatrix();
			     String current_status = decimalformat.format(100 - finalResult) + " %" + TextFormatting.RESET ;
	             this.getFontRenderer().drawString(current_status,width + 64 - 5,  slotarmor * 15 + height - 50 + 7, 0);
	             this.getFontRenderer().drawString(current_status,width + 64 - 7,  slotarmor * 15 + height - 50 + 7, 0);
	             this.getFontRenderer().drawString(current_status,width + 64 - 6,  slotarmor * 15 + height - 50 + 8, 0);
	             this.getFontRenderer().drawString(current_status,width + 64 - 6,  slotarmor * 15 + height - 50 + 6, 0);
	             this.getFontRenderer().drawString(colors_degrement[stage] + current_status,width + 64 - 6, slotarmor * 15 + height - 50 + 7, 0);
			 }		     		
		 }
		 if (this.mc.gameSettings.ArmorHudDesign == 0)
		 {
    		this.renderSideArmor(i, 2, 66, false);
		 }
		 if (this.mc.gameSettings.ArmorHudDesign == 2)
		 {
	        Collection activepotion = this.mc.thePlayer.getActivePotionEffects();
	        PotionEffect potionEffect;
	        Potion potion = null;
	        if (!activepotion.isEmpty())
	        {
	            for (Iterator potionsIterator = this.mc.thePlayer.getActivePotionEffects().iterator(); potionsIterator.hasNext();)
	            {
	                 potionEffect = (PotionEffect)potionsIterator.next();
	                 potion = potionEffect.getPotion();
	            }
	        }
	    	this.renderSideArmor(i, this.mc.thePlayer.isPotionActive(potion) ? i - 102 : i - 70, 38, true);
		 }
	 }	
    
	/* UNE SOUS-M�THODE POUR FAIRE LE RENDU DANS LES COT� - ELLE EST S�PAR� CAR J'AI BESOIN DES PARAM�TRES. */ 
	 public void renderSideArmor(float f, int width , int height, boolean right_side)
	 {
     	if(this.mc.gameSettings.armor)
     	{  
     		for (int slotarmor = 0; slotarmor < 4; ++slotarmor)
     		{
     	        ItemStack itemStack = this.mc.thePlayer.inventory.armorItemInSlot(4 - slotarmor - 1);
     			
     			if (itemStack == null)
     			{
     				continue;
     			}
     			
     			double damage = new Double(itemStack.getMaxDamage());
     			double result = itemStack.getItemDamage() / damage;
     		    double finalResult = result * 100;
     		    DecimalFormat decimalformat = new DecimalFormat("###");
                int[] frame  = {20, 40, 60, 80};
                String[] colors_degrement = {"�4", "�c", "�6", "�a", "�2"};
                int stage = 0;
                
                for (stage = 0; stage < frame.length; ++stage)
                {
                    if (100 - finalResult < frame[stage])
                    {
                        break;
                    }
                }

                String percent = decimalformat.format(100 - finalResult) + " %" + TextFormatting.RESET ;
                int var9 = 8453920;
                this.getFontRenderer().drawString(" " + percent,width + 17, slotarmor * 17 + height + 4, 0);
                this.getFontRenderer().drawString(" " + percent,width + 15, slotarmor * 17 + height + 4, 0);
                this.getFontRenderer().drawString(" " + percent,width + 16, slotarmor * 17 + height + 5, 0);
                this.getFontRenderer().drawString(" " + percent,width + 16, slotarmor * 17 + height + 3, 0);
                this.getFontRenderer().drawString(colors_degrement[stage] + " " + percent,width + 16, slotarmor * 17 + height + 4, var9);
     			GlStateManager.pushMatrix();
	            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
	            this.renderItemGui(itemStack,right_side ? width + 50 : width, slotarmor * 17 + height, f, true);
	      	    GlStateManager.popMatrix();
     		}
     	}
	 }
	 
	 /** LA M�THODE PRINCIPAL DE LA CLASSE. C'EST LE LOOP */
    public void renderGameOverlay(float partialTicks)
    {
     	if (this.mc.gameSettings.PotionRenderer)
      	{
            this.mc.guiPotionAnimation.updatePotionWindow();
            this.renderPotion();
      	}
       	else
       	{
			/** DEPUIS LA 1.9, MINECRAFT INT�GRE UN INDICATEUR DE POTION! UN BOOLEAN (PotionRenderer) PERMET A L'UTILISATEUR DE CHOISIR ENTRE LES DEUX*/
            this.renderPotionEffects(scaledresolution);
       	}

        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.disableLighting();
        GlStateManager.enableAlpha();
		
		/** INT�GRE L'INDICATEUR D'ARMURE DANS LA LOOP*/
        if (this.mc.gameSettings.armor)
     	{
     		this.renderArmor();
     	}
		
         RenderHelper.disableStandardItemLighting();
         GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
         GlStateManager.disableLighting();
         GlStateManager.enableAlpha();

		 /** FAIT LE RENDU DU JOUEUR EN 3D - LE DERNIER ARGUMENT EST (EntityLivingBase) this.mc.getRenderViewEntity() CAR QUAND ON EST EN MODE SP�CTATEUR ET QU'ON EST DANS UN ENTITY, LE JEU PREND L'ENTIT� QUE TU EST. */
		 
 	    if (this.mc.thePlayer.isSprinting() || this.mc.thePlayer.isSneaking() || this.mc.thePlayer.capabilities.isFlying || this.mc.gameSettings.alwayshowplayer ||  this.mc.thePlayer instanceof EntityLivingBase && ((EntityLivingBase)this.mc.thePlayer).isElytraFlying()){
 	    	if(this.mc.gameSettings.playerRenderer && !this.mc.gameSettings.showDebugInfo){
 	    	    if (!this.mc.isGamePaused()){    	    	   
 	    	    	 drawEntityOnScreen(30, this.mc.displayHeight / 100 + 48, 20, -30.0F, 1.0F,(EntityLivingBase) this.mc.getRenderViewEntity());
 	    	       }
 	    		}
 	    	} 
    }
	
    public void drawEntityOnScreen(int p_147046_0_, int p_147046_1_, int p_147046_2_, float p_147046_3_, float p_147046_4_, EntityLivingBase p_147046_5_)
    {
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)p_147046_0_, (float)p_147046_1_, 50.0F);
        GlStateManager.scale((float)(-p_147046_2_), (float)p_147046_2_, (float)p_147046_2_);
        GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        float var6 = p_147046_5_.renderYawOffset;
        float var7 = p_147046_5_.rotationYaw;
        float var8 = p_147046_5_.rotationPitch;
        float var9 = p_147046_5_.prevRotationYawHead;
        float var10 = p_147046_5_.rotationYawHead;
        GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
		
        if (this.mc.thePlayer instanceof EntityLivingBase && ((EntityLivingBase)this.mc.thePlayer).isElytraFlying())
        {
            GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
            GlStateManager.rotate(-((float)Math.atan((double)(0 / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
            p_147046_5_.renderYawOffset = 0;
            p_147046_5_.rotationYaw = (float)Math.atan((double)(p_147046_3_ / 40.0F)) * 40.0F;
            p_147046_5_.rotationPitch = 0;
            p_147046_5_.rotationYawHead = 0;
            p_147046_5_.prevRotationYawHead = 0;
        }
        else
        {
            GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
            GlStateManager.rotate(-((float)Math.atan((double)(p_147046_4_ / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
            p_147046_5_.renderYawOffset = (float)Math.atan((double)(p_147046_3_ / 40.0F)) * 20.0F;
            p_147046_5_.rotationYaw = (float)Math.atan((double)(p_147046_3_ / 40.0F)) * 40.0F;
            p_147046_5_.rotationPitch = ((float)Math.atan((double)(var8 / 40.0F))) * 20.0F;
            p_147046_5_.rotationYawHead = p_147046_5_.rotationYaw;
            p_147046_5_.prevRotationYawHead = p_147046_5_.rotationYaw;
        }	
        
        GlStateManager.translate(0.0F, 0.0F, 0.0F);
        RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
        rendermanager.setPlayerViewY(180.0F);
        rendermanager.setRenderShadow(false);
        rendermanager.doRenderEntity(p_147046_5_, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F, false);
        rendermanager.setRenderShadow(true);
        p_147046_5_.renderYawOffset = var6;
        p_147046_5_.rotationYaw = var7;
        p_147046_5_.rotationPitch = var8;
        p_147046_5_.prevRotationYawHead = var9;
        p_147046_5_.rotationYawHead = var10;
        GlStateManager.popMatrix();
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableRescaleNormal();
        GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.disableTexture2D();
        GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }
	
	/** FAIT LE RENDU DES POTIONS AVEC LES ANIMATIONS. */
    private void renderPotion()
    {
        ScaledResolution scaledresolution = new ScaledResolution(this.mc);
        int width = this.mc.gameSettings.Potionx16 ? scaledresolution.getScaledWidth() - 35 : scaledresolution.getScaledWidth() - 120;
        int height = scaledresolution.getScaledHeight() / 100 + 30;   
        Collection activepotion = this.mc.thePlayer.getActivePotionEffects();
        
        if (!activepotion.isEmpty())
        {
            int width2 = 33;
            for (Iterator potionsIterator = this.mc.thePlayer.getActivePotionEffects().iterator(); potionsIterator.hasNext(); height += width2)
            {
                PotionEffect potionEffect = (PotionEffect)potionsIterator.next();
                Potion potion = potionEffect.getPotion();
                
            	if (!this.mc.guiPotionAnimation.getCurrentState() && potionEffect.getDuration() > 30)
            	{
					this.guiPotionAnimation.setPotionInstance(false, true);
            	}
            	
                
            	if (this.mc.guiPotionAnimation.getCurrentState() && potionEffect.getDuration() < 30)
            	{
					this.guiPotionAnimation.setPotionInstance(true, false);
            	}
            } 
        }
        else
        {
        	if (!this.mc.guiPotionAnimation.getCurrentState())
        	{
				this.guiPotionAnimation.setPotionInstance(true, false);
        	}
        }
    }
}
